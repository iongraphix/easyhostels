package com.dominic.okenwa.easyhostels;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.dominic.okenwa.easyhostels.activity.LoginActivity;
import com.dominic.okenwa.easyhostels.activity.SettingsActivity;
import com.dominic.okenwa.easyhostels.fragments.ExplorerFragment;
import com.dominic.okenwa.easyhostels.fragments.FavoritesFragment;
import com.dominic.okenwa.easyhostels.util.EasyConfig;
import com.dominic.okenwa.easyhostels.util.EasyUser;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, DrawerLayout.DrawerListener {
    // Butterknife Binds
    @Bind(R.id.drawer_layout) DrawerLayout drawer;
    @Bind(R.id.nav_view) NavigationView navigationView;

    public ExplorerFragment explorer_frag;
    public FragmentManager fm;

    public MainActivity() {
        this.explorer_frag = new ExplorerFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        preChecks();
        setContentView(R.layout.activity_main);
        String mode = null;
        if(savedInstanceState != null){
            mode = savedInstanceState.getString("mode");
        }
        ButterKnife.bind(this);
        setDefaultExplorerFragment();
        configureNavigation();
        configureDrawer();
    }

    private void preChecks() {
        // TODO: MainActivity Prechecks
    }

    private void setDefaultExplorerFragment() {
        explorer_frag = ExplorerFragment.newInstance(drawer, this.getApplicationContext());
        fm = getSupportFragmentManager();
        fm.beginTransaction().setCustomAnimations(R.anim.fade_in, R.anim.fade_out).replace(R.id.mainframe, explorer_frag, "EXPLORE").commit();
    }

    private void configureDrawer() {
        drawer.addDrawerListener(this);
    }

    private void configureNavigation() {
        View hview  = navigationView.getHeaderView(0);

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_explore);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            FavoritesFragment myFragment = (FavoritesFragment)fm.findFragmentByTag("FAV");
            if (myFragment != null && myFragment.isVisible()) {
                this.navigationView.setCheckedItem(R.id.nav_explore);
                this.fm.beginTransaction().setCustomAnimations(R.anim.fade_in,R.anim.fade_out).replace(R.id.mainframe, ExplorerFragment.newInstance(this.drawer, this.getApplicationContext()),"EXPLORE").commit();
            }
            else {
                if(ExplorerFragment.showing_sweet){
                    ExplorerFragment.sweetme.dismiss();
                    ExplorerFragment.showing_sweet = false;
                }
                else {
                    AlertDialogWrapper.Builder builder = new AlertDialogWrapper.Builder(this);
                    builder.setTitle(R.string.confirm_exit)
                            .setPositiveButton(R.string.positive, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    MainActivity.this.finish();
                                }
                            })
                            .setNegativeButton(R.string.negative, null)
                            .show();
                }
            }


        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favorites, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    public void showEmailIntent(String to, String title, String msg) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", to, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, title);
        emailIntent.putExtra(Intent.EXTRA_TEXT, msg);
        startActivity(Intent.createChooser(emailIntent, "Help and Feedback"));
    }

    public void showDonationPage(String link) {
        String url = link;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void showSettings() {
        Intent iset = new Intent(this.getApplicationContext(), SettingsActivity.class);
        startActivity(iset);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Fragment fragment = null;
        FragmentManager fm;
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_explore:
                fragment = ExplorerFragment.newInstance(this.drawer, this.getApplicationContext());
                this.fm.beginTransaction().setCustomAnimations(R.anim.fade_in, R.anim.fade_out).replace(R.id.mainframe, fragment, "EXPLORE").commit();
                break;
            case R.id.nav_fav:
                fragment = FavoritesFragment.newInstance(this, this.drawer);
                this.fm.beginTransaction().replace(R.id.mainframe, fragment, "FAV").commit();
                break;
            case R.id.nav_settings:
                showSettings();
                navigationView.setCheckedItem(R.id.nav_explore);
                break;
            case R.id.nav_email:
                showEmailIntent("Dominic.imakestuff@gmail.com", "Feedback", "Type Here");
                break;
            case R.id.nav_donate:
                showDonationPage(EasyConfig.DONATE_URL);
                break;
            default:
                // NOP
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {
        this.explorer_frag.openSearchView();
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        this.explorer_frag.restoreSearchView();
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }
}
