package com.dominic.okenwa.easyhostels.intro;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.dominic.okenwa.easyhostels.R;
import com.dominic.okenwa.easyhostels.activity.LoginActivity;
import com.dominic.okenwa.easyhostels.fragments.SampleSlide;
import com.github.paolorotolo.appintro.AppIntro;

public class QuickIntro extends AppIntro {
    @Override
    public void init(Bundle savedInstanceState) {
        addSlide(SampleSlide.newInstance(R.layout.intro));
        addSlide(SampleSlide.newInstance(R.layout.intro2));
        addSlide(SampleSlide.newInstance(R.layout.intro3));
        addSlide(SampleSlide.newInstance(R.layout.intro4));
    }

    private void loadMainActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onNextPressed() {
    }

    @Override
    public void onSkipPressed() {
        loadMainActivity();
        Toast.makeText(getApplicationContext(),
                getString(R.string.skip), Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onDonePressed() {
        loadMainActivity();
        finish();
    }

    @Override
    public void onSlideChanged() {
    }

    public void getStarted(View v){
        loadMainActivity();
        finish();
    }

}
