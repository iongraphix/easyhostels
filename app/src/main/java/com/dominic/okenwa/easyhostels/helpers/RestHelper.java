package com.dominic.okenwa.easyhostels.helpers;

import android.app.Activity;
import android.os.StrictMode;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.dominic.okenwa.easyhostels.models.MarkerData;
import com.dominic.okenwa.easyhostels.util.EasyConfig;
import com.dominic.okenwa.easyhostels.util.MarkerResourceGenerator;
import com.github.clans.fab.FloatingActionButton;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import sexy.code.Callback;
import sexy.code.HttPizza;
import sexy.code.Request;
import sexy.code.Response;

/**
 * Created by live on 5/8/16.
 */
public class RestHelper {
    public static void enableStrictMode()
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
    }

    public static void test(){

        String url = "http://vvuconnect.net/e-service/resource/hostel";
        HttPizza client = new HttPizza();

        Request request = client.newRequest()
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(Response response) {
                String data = null;
                try {
                    data = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (response.isSuccessful()) {
                    Log.w(EasyConfig.TAG, "Body: " + data);

                } else {
                    Log.w(EasyConfig.TAG, "Body: " + data);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.w(EasyConfig.TAG, "Body: " + t.getMessage());
            }
        });
    }


    // First Get Request Test We did with HttPizza
    public static void doRequest(final FloatingActionButton fb, final SupportMapFragment mapFragment, final FragmentActivity activity){
        enableStrictMode();
        HttPizza client = new HttPizza();

        Request request = client.newRequest()
                .url(EasyConfig.REST_URL+"resource/hostel")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(Response response) {
                if (response.isSuccessful()) {
                    try {
                        String data = response.body().string();
                        Log.w(EasyConfig.TAG, "Body: " + data);

                        RestStatic.hostels_str = data;
                        createMarkersFromJson(data, mapFragment.getMap(), activity);
                        fb.setIndeterminate(false);

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    fb.setIndeterminate(false);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Snackbar.make(null, "Please Check Internet Connection", Snackbar.LENGTH_SHORT).show();
                fb.setIndeterminate(false);
            }
        });

    }

    // This is a generic function that takes JSON String a turns the into beautiful markers
    public static void createMarkersFromJson(String json, GoogleMap map, Activity act) throws JSONException {
        // De-serialize the JSON string into an array of city objects
        JSONArray jsonArray = new JSONArray(json);
        ArrayList<Marker> markers  = new ArrayList<Marker>();
        HashMap<Marker, MarkerData> data = new HashMap<Marker, MarkerData>();

        if(RestStatic.markers != null){
            for(Marker m : RestStatic.markers){
                m.remove();
            }
        }
        RestStatic.map = null;

        if(jsonArray.length() == 0){
            Snackbar.make(act.getCurrentFocus(),"Empty Result Set. Please Pan Around", Snackbar.LENGTH_LONG).show();

            return;
        }


        for (int i = 0; i < jsonArray.length(); i++) {
            // Create a marker for each city in the JSON data.
            JSONObject jsonObj = jsonArray.getJSONObject(i);
            MarkerData dat = new MarkerData();
            Marker m = map.addMarker(new MarkerOptions()
                            .title(jsonObj.getString("display_name"))
                            .snippet("Tap for more details")
                            .position(new LatLng(
                                    jsonObj.getDouble("lat"),
                                    jsonObj.getDouble("lng")
                            )).icon(BitmapDescriptorFactory.fromBitmap(MarkerResourceGenerator.getMarker(act.getApplicationContext(),String.valueOf(jsonObj.getDouble("display_price")),"-",false)))
            );
            dat.setId(jsonObj.getInt("id"));
            dat.setDisplay_name(jsonObj.getString("display_name"));
            dat.setShort_description(jsonObj.getString("short_description"));
            String bl = jsonObj.getString("available");
            if(bl == "1"){
                dat.setAvailable(true);
            }
            else if(bl == "0"){
                dat.setAvailable(false);
            }
            dat.setContact_email(jsonObj.getString("contact_email"));
            dat.setContact_phone(jsonObj.getString("contact_phone"));
            dat.setAddress(jsonObj.getString("address"));
            dat.setLat(jsonObj.getDouble("lat"));
            dat.setLng(jsonObj.getDouble("lng"));
            dat.setDisplay_price(jsonObj.getDouble("display_price"));
            dat.setNotes(jsonObj.getString("notes"));
            dat.setCreated_at(jsonObj.getString("created_at"));
            dat.setUpdated_at(jsonObj.getString("updated_at"));

            data.put(m, dat);
            markers.add(m);
        }
        RestStatic.map = null;
        RestStatic.map = data;
        RestStatic.markers = markers;

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker m1: markers) {
            builder.include(m1.getPosition());
        }
        LatLngBounds bounds = builder.build();
        int padding = 240; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        map.animateCamera(cu, 6000, null);
    }

    // This function takes care of Radius Search with a filter
    public static void getHostelsByRadiusFilter(String radius,LatLng whereIam,String min,String max,final FloatingActionButton fb, final SupportMapFragment mapFragment, final FragmentActivity activity) {

        enableStrictMode();
        HttPizza client = new HttPizza();

        Request request = client.newRequest()
                .url(EasyConfig.REST_URL + EasyConfig.REST_URL + "/resource/find/radius/filter/"+min+"/"+max+"/" + whereIam.latitude + "/" + whereIam.longitude+ "/" + radius)
                .build();
        String myurl = EasyConfig.REST_URL + "resource/find/radius/"+min+"/"+max+"/" + whereIam.latitude + "/" + whereIam.longitude+ "/" + radius;
        Log.w(EasyConfig.TAG, "try this" + myurl);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(Response response) {
                if (response.isSuccessful()) {
                    try {
                        String data = response.body().string();
                        Log.w(EasyConfig.TAG, "Body: " + data);
                        RestStatic.hostels_str = data;
                        createMarkersFromJson(data, mapFragment.getMap(), activity);
                        fb.setIndeterminate(false);

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    fb.setIndeterminate(false);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Snackbar.make(null, "Please Check Internet Connection", Snackbar.LENGTH_SHORT).show();
                fb.setIndeterminate(false);
            }
        });
    }

    // This function takes care of Radius Search
    public static void getHostelsByRadius(String radius,LatLng whereIam,final FloatingActionButton fb, final SupportMapFragment mapFragment, final FragmentActivity activity) {

        enableStrictMode();
        HttPizza client = new HttPizza();

        Request request = client.newRequest()
                .url(EasyConfig.REST_URL + "resource/find/radius/" + whereIam.latitude + "/" + whereIam.longitude + "/" + radius)
                .build();
        String myurl = EasyConfig.REST_URL + "resource/find/radius/" + whereIam.latitude + "/" + whereIam.longitude+ "/" + radius;
        Log.w(EasyConfig.TAG, "try this: "+myurl);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(Response response) {
                if (response.isSuccessful()) {
                    try {
                        String data = response.body().string();
                        Log.w(EasyConfig.TAG, "Body: " + data);
                        RestStatic.hostels_str = data;
                        createMarkersFromJson(data, mapFragment.getMap(), activity);
                        fb.setIndeterminate(false);

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    fb.setIndeterminate(false);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Snackbar.make(null, "Please Check Internet Connection", Snackbar.LENGTH_SHORT).show();
                fb.setIndeterminate(false);
            }
        });
    }

    // This function takes care of Bounds Search with a filter
    public static void getHostelsByBoundsFilter(String min, String max, LatLngBounds lookingAt,final FloatingActionButton fb, final SupportMapFragment mapFragment, final FragmentActivity activity) {

        enableStrictMode();
        HttPizza client = new HttPizza();

        Request request = client.newRequest()
                .url(EasyConfig.REST_URL + "resource/find/bounds/filter/" + min + "/" + max + "/" + lookingAt.southwest.latitude + "/" + lookingAt.southwest.longitude + "/" + lookingAt.northeast.latitude+"/"+lookingAt.northeast.longitude+"")
                .build();
        String myurl = EasyConfig.REST_URL + "resource/find/bounds/filter/" + min + "/" + max + "/" + lookingAt.southwest.latitude + "/" + lookingAt.southwest.longitude + "/" + lookingAt.northeast.latitude+"/"+lookingAt.northeast.longitude + "";
        Log.w(EasyConfig.TAG, "try this: " + myurl);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(Response response) {
                if (response.isSuccessful()) {
                    try {
                        String data = response.body().string();
                        Log.w(EasyConfig.TAG, "Body: " + data);
                        RestStatic.hostels_str = data;
                        createMarkersFromJson(data, mapFragment.getMap(), activity);
                        fb.setIndeterminate(false);

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    fb.setIndeterminate(false);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Snackbar.make(null, "Please Check Internet Connection", Snackbar.LENGTH_SHORT).show();
                fb.setIndeterminate(false);
            }
        });
    }

    // This function takes care of Bounds Search
    public static void getHostelsByBounds(LatLngBounds lookingAt,final FloatingActionButton fb, final SupportMapFragment mapFragment, final FragmentActivity activity) {

        enableStrictMode();
        HttPizza client = new HttPizza();
        if(lookingAt == null){
            Snackbar.make(activity.getCurrentFocus(), "Please Wait. Still Loading Map", Snackbar.LENGTH_SHORT).show();
            fb.setIndeterminate(false);
            return;
        }
        Request request = client.newRequest()
                .url(EasyConfig.REST_URL + "resource/find/bounds/" + lookingAt.southwest.latitude + "/" + lookingAt.southwest.longitude + "/" + lookingAt.northeast.latitude + "/" + lookingAt.northeast.longitude+"")
                .build();
        String myurl = EasyConfig.REST_URL + "resource/find/bounds/"+ lookingAt.southwest.latitude + "/" + lookingAt.southwest.longitude + "/" + lookingAt.northeast.latitude+"/"+lookingAt.northeast.longitude + "";
        Log.w(EasyConfig.TAG, "try this: "+myurl);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(Response response) {
                if (response.isSuccessful()) {
                    try {
                        String data = response.body().string();
                        Log.w(EasyConfig.TAG, "Body: " + data);
                        RestStatic.hostels_str = data;
                        createMarkersFromJson(data, mapFragment.getMap(), activity);
                        fb.setIndeterminate(false);

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    fb.setIndeterminate(false);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Snackbar.make(null, "Please Check Internet Connection", Snackbar.LENGTH_SHORT).show();
                fb.setIndeterminate(false);
            }
        });
    }

}
